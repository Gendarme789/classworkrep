import random
from day27_animals import *

class World:

    def __init__(self, xsize, ysize, numfish, numbears):
        self.grid = []
        self.maxX = xsize
        self.maxY = ysize

        # Create the grid with no animals
        for irow in range(self.maxY):
            row = []
            for icol in range(self.maxX):
                row.append(None)
            self.grid.append(row)

        # Randomly create initial fish and bears.

        # First, we create a list of all positions in the grid.
        positions = []
        for i in range(self.maxX):
            for j in range(self.maxY):
                positions.append((i,j))
        # Next, we use random.sample to take numfish + numbears elements from the positions list.
        randpos = random.sample(positions, numfish + numbears)
        # The fish locations are the first half and the bears the second half of the positions.
        fish = randpos[0:numfish-1]
        bears = randpos[numfish:]

        for point in fish:
            x = point[0]
            y = point[1]
            self.grid[y][x] = Fish(x, y, self)
        for point in bears:
            x = point[0]
            y = point[1]
            self.grid[y][x] = Bear(x, y, self)


    def is_empty(self, x, y):
        "Test if a location is empty."
        if x < 0 or x >= self.maxX:
            return False
        if y < 0 or y >= self.maxY:
            return False
        return self.grid[y][x] == None

    def move(self, animal, x, y):
        "Move an animal from its current location to the new location"

        self.grid[animal.y][animal.x] = None
        self.grid[y][x] = animal

    def add_animal(self, animal):
        "Add an animal to the world"
        self.grid[animal.y][animal.x] = animal

    def has_fish(self, x, y):
        "Returns true if the given location has a fish"
        if x < 0 or x >= self.maxX:
            return False
        if y < 0 or y >= self.maxY:
            return False
        return isinstance(self.grid[y][x], Fish)

    def kill_animal(self, x, y):
        "Remove an animal from the grid"
        self.grid[y][x].kill()
        self.grid[y][x] = None

    def draw(self):
        "Draw the animals"

        for i in range(self.maxY):
            for j in range(self.maxX):
                if self.grid[i][j]:
                    self.grid[i][j].draw()

    def live_a_little(self):
        "Have all the animals live a little"

        for i in range(self.maxY):
            for j in range(self.maxX):
                if self.grid[i][j]:
                    self.grid[i][j].live_a_little()
