class bank_account():

    def __init__(self, name, ID):
        self.account_holder = str(name)
        self.ID_number = int(ID)
        self.account_balance = 0.00

    def show_account_info(self):
        x = "Account holder: " + str(self.account_holder) + "    ID number: " + str(self.ID_number) + "    Account balance: $" + str(self.account_balance)
        return x

    def withdraw_money(self, amount):
        if amount > self.account_balance:
            return "ERROR: Amount to withdraw larger than balance!"
        else:
            self.account_balance = self.account_balance - amount
            return("Your balance is now $" + str(self.account_balance))

    def deposit_money(self, amount):
        self.account_balance = self.account_balance + amount
        return("Your balance is now $" + str(self.account_balance))

    def transfer_money(self, account, amount):
        if amount > self.account_balance:
            return "ERROR: Amount to transfer larger than balance!"
        else:
            self.account_balance = self.account_balance - amount
            account.account_balance = account.account_balance + amount
            return("Your balance is now $" + str(self.account_balance))
            
bank1 = bank_account("Martin", 1025)
print(bank1.show_account_info())
bank2 = bank_account("John", 1029)
print(bank2.show_account_info())
print(bank1.deposit_money(300.0))
print(bank1.show_account_info())
print(bank1.transfer_money(bank2, 150.0))
print(bank1.show_account_info())
print(bank2.show_account_info())
