import os
import json

#Each book is a dictionary with the following entries
#    key "avail", value a boolean if the book is available
#    key "num", value a unique integer for the book
#    key "title", value a string which is the title

def show_menu():
    "shows the menu, prompts for a choice, and returns the number"

    print("Welcome to our library!  Choose")
    print("  0. leave the program and save books")
    print("  1. show all books in the collection")
    print("  2. add a new book to the collection")
    print("  3. check out a book of the library")
    print("  4. return a book to the library")
    c = input("Type 0, 1, 2, 3, or 4 : ")
    return c

def show_books(books):
    "shows the books currently in the collection"

    print("") # Print a blank line
    for book in books:
        s = str(book["num"]) + ' ' + book["title"] + ' by: ' + book["author"]

        # Now the available
        if book["avail"]:
            s += ' available'
        else:
            s += ' checked out'
        print(s)

    print("") # Print a blank line

def add_book(books):
    "adds a book to the collection"

    # Find max used num
    maxnum = 0
    for book in books:
        if book["num"] > maxnum:
            maxnum = book["num"]

    title = input('Enter a title: ')
    author = input('Enter an author: ')
    
    # Add the book with key one larger than the max key
    books.append({"avail": True, "num": maxnum+1, "title": title, "author": author})

def checkout(books):
    "checks out a book"
    show_books(books)
    n = int(input('Enter book number: '))
    for book in books:
        if book["num"] == n:
            if not book["avail"]:
                print("Book is already checked out")
            else:
                book["avail"] = False
                
def checkin(books):
    "return a book to the library"
    show_books(books)
    n = int(input('Enter book number: '))
    for book in books:
        if book["num"] == n:
            if book["avail"]:
                print("Book is not checked out")
            else:
                book["avail"] = True

def main():
    # If the books.json file exists, load it.  Otherwise start with an
    # empty list of books
    if os.path.exists("books.json"):
        with open("books.json") as f:
            books = json.load(f)
    else:
        books = []

    while True:
        choice = show_menu()
        if choice == "0": break
        if choice == "1": show_books(books)
        if choice == "2": add_book(books)
        if choice == "3": checkout(books)
        if choice == "4": checkin(books)

    # Note the "w" tells the operating system we want to write to the file.
    # The "w" also causes the file (if it exists) to be replaced.
    with open("books.json", "w") as f:
        json.dump(books, f)

main()
