import day22_cluster
import turtle
import csv

# Some sample data
x = 0
points = []
with open('drt.csv', newline='') as csvfile:
    rowsa = csv.reader(csvfile)
    newrowsa = rowsa
    for rowa in newrowsa:
        x = rowa[0]
        l = [0, 0]
        first = x[0:3].replace("'", "")
        first = float(first)
        second = x[8:11].replace("'", "")
        second = float(second)
        l[0] = first
        l[1] = second
        l = tuple(l)
        points.append(l)

centroids = [(31, 5), (34, 20), (49, 12)]

colors = ["red", "green", "blue"]

turtle.setworldcoordinates(20, 0, 60, 40)
turtle.hideturtle()
turtle.up()

# Draw the points and the initial centroids
for p in points:
    turtle.goto(p[0], p[1])
    turtle.dot()
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(10)

input("Press enter to continue")

# Classify the points
classified_points = day22_cluster.classify_points(points, centroids)

# draw the classification
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

# Run a single iteration
centroids, classified_points = day22_cluster.kmeans(3, points, 2, centroids)

# draw the new centroids
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(14)

input("Press enter to continue")

# Draw the new classification
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

input("Press enter to continue")

# clear the screen
turtle.clear()

# Run 20 iterations starting from random points
centroids, classified_points = day22_cluster.kmeans(3, points)

# draw the classification
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(14)
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

input("Press enter to exit")
