import random
import csv

class World:

    def __init__(self):
        self.map = [[None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
                    [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]]

    def test_position(self, x, y):
        return(self.map[19-y][x])

    def add_attack_robot(self, team, x, y, direction):
        if self.map[y][x] == None:
            self.map[y][x] = AttackRobot(x, y, direction, team)
            return(1)
        else:
            return(0)

    def add_medic_robot(self, team, x, y, direction, filename):
        if self.map[y][x] == None:
            self.map[y][x] = MedicRobot(x, y, direction, team)
            self.map[y][x].initialize_pattern(filename)
            return(1)
        else:
            return(0)

    def kill_robot(self, x, y):
        self.map[y][x] = None
    
    def move_robot(self, oldx, oldy, newx, newy):
        self.map[newx][newy] = self.map[oldx][oldy]
        self.map[oldx][oldy] = None

    def game_over(self):
        team_1 = 0
        team_2 = 0
        for row in range(20):
            for column in range(20):
                if (self.map[row][column] != None):
                    team = self.map[row][column].get_team()
                    if team == 1:
                        team_1 = team_1 + 1
                    else:
                        team_2 = team_2 + 1
                else:
                    pass
        if team_1 == 0 or team_2 == 0:
            if team_1 > 0:
                return(1)
            elif team_2 > 0:
                return(2)
        else:
            return(0)

    def run_turn(self, world):
        for row in range(20):
            for column in range(20):
                if self.map[row][column] != None:
                    self.map[row][column].robotstate = 0
        for row in range(20):
            for column in range(20):
                if self.map[row][column] != None:
                    if self.map[row][column].robotstate == 0:
                        self.map[row][column].run_turn(world)
class Robot:
    def __init__(self, x, y, direction, team):
        self.x = x
        self.y = y
        self.direction = str(direction)
        self.team = team
        self.health = 50
        self.robotstate = 0

    def get_team(self):
        return(self.team)

    def get_direction(self):
        return(self.direction)

    def get_health(self):
        return(self.health)

    def healthchange(self, world, hp):
        self.health = self.health + hp
        if self.health < 0:
            world.kill_robot(self.x, self.y)
        elif self.health > 50:
            self.health = 50

    def robotstate(self, state):
        self.robotstate = state

    def robot_infront(self):
        x = self.x
        y = self.y
        if self.direction == 'N':
            if self.y == 0:
                return(0)
            elif self.y > 0:
                if world.map[y - 1][x] != None:
                    if world.map[y - 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'E':
            if self.x == 19:
                return(0)
            elif self.x < 19:
                if world.map[y][x + 1] != None:
                    if world.map[y][x + 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'S':
            if self.y == 19:
                return(0)
            elif self.y < 19:
                if world.map[y + 1][x] != None:
                    if world.map[y + 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'W':
            if self.x == 0:
                return(0)
            elif self.x > 0:
                if world.map[y][x - 1] != None:
                    if world.map[y][x - 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)

    def robot_to_right(self):
        x = self.x
        y = self.y
        if self.direction == 'N':
            if self.x == 19:
                return(0)
            elif self.x < 19:
                if world.map[y][x + 1] != None:
                    if world.map[y][x + 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'E':
            if self.y == 19:
                return(0)
            elif self.y < 19:
                if world.map[y + 1][x] != None:
                    if world.map[y + 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'S':
            if self.x == 0:
                return(0)
            elif self.x > 0:
                if world.map[y][x - 1] != None:
                    if world.map[y][x - 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'W':
            if self.y == 0:
                return(0)
            elif self.y > 0:
                if world.map[y - 1][x] != None:
                    if world.map[y - 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)

    def robot_to_left(self):
        x = self.x
        y = self.y
        if self.direction == 'N':
            if self.x == 0:
                return(0)
            elif self.x > 0:
                if world.map[y][x - 1] != None:
                    if world.map[y][x - 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'E':
            if self.y == 0:
                return(0)
            elif self.y > 0:
                if world.map[y - 1][x] != None:
                    if world.map[y - 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'S':
            if self.x == 19:
                return(0)
            elif self.x < 19:
                if world.map[y][x + 1] != None:
                    if world.map[y][x + 1].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)
        elif self.direction == 'W':
            if self.y == 19:
                return(0)
            elif self.y < 19:
                if world.map[y + 1][x] != None:
                    if world.map[y + 1][x].get_team == 1:
                        return(1)
                    else:
                        return(2)
                else:
                    return(0)

    def left90(self):
        if self.direction == 'N':
            self.direction = 'W'
        elif self.direction == 'E':
            self.direction = 'N'
        elif self.direction == 'S':
            self.direction = 'E'
        elif self.direction == 'W':
            self.direction = 'S'
            
    def right90(self):
        if self.direction == 'N':
            self.direction = 'E'
        elif self.direction == 'E':
            self.direction = 'S'
        elif self.direction == 'S':
            self.direction = 'W'
        elif self.direction == 'W':
            self.direction = 'N'

    def forward(self):
        if self.direction == 'N':
            if self.y == 0:
                self.direction = 'S'
            elif self.y > 0:
                self.y = self.y - 1
        elif self.direction == 'E':
            if self.x == 19:
                self.direction = 'W'
            elif self.x < 19:
                self.x = self.x + 1
        elif self.direction == 'S':
            if self.y == 19:
                self.direction = 'N'
            elif self.y < 19:
                self.y = self.y + 1
        elif self.direction == 'W':
            if self.x == 0:
                self.direction = 'E'
            elif self.x > 0:
                self.x = self.x - 1

    def move(self, world):
        old = world.map[self.y][self.x]
        world.map[self.y][self.x] = None
        buffer = random.randrange(1, 5)
        if buffer == 1 or buffer == 2:
            self.forward()
        elif buffer == 3:
            self.left90()
        elif buffer == 4:
            self.right90()
        world.map[self.y][self.x] = old

class AttackRobot(Robot):
    
    def attack(self, enemy, world):
        enemy.healthchange(world, -random.randrange(1,7))

    def run_turn(self, world):
        front = self.robot_infront() 
        left = self.robot_to_left()
        right = self.robot_to_right()
        if front != 0 and front != self.get_team():
            x = self.x
            y = self.y
            if self.direction == 'N':
                self.attack(world.map[y - 1][x], world)
            elif self.direction == 'E':
                self.attack(world.map[y][x + 1], world)
            elif self.direction == 'S':
                self.attack(world.map[y + 1][x], world)
            elif self.direction == 'W':
                self.attack(world.map[y][x - 1], world)
        elif front == self.get_team:
            pass
        elif right != 0 and right != self.get_team():
            if self.direction == 'N':
                self.direction = 'E'
            elif self.direction == 'E':
                self.direction = 'S'
            elif self.direction == 'S':
                self.direction = 'W'
            elif self.direction == 'W':
                self.direction = 'N'
        elif left != 0 and left != self.get_team():
            if self.direction == 'N':
                self.direction = 'W'
            elif self.direction == 'E':
                self.direction = 'N'
            elif self.direction == 'S':
                self.direction = 'E'
            elif self.direction == 'W':
                self.direction = 'S'
        else:
            self.move(world)
        self.robotstate = 1


class MedicRobot(Robot):
    step = 0
    pointr = 0
    
    def heal_other(self):
        front = self.robot_infront() 
        left = self.robot_to_left()
        right = self.robot_to_right()
        if right == self.get_team():
            add_health = random.randrange(0, 4)
            if self.direction == 'N':
                world.map[self.y][self.x + 1].healthchange(world, add_health)
            elif self.direction == 'E':
                world.map[self.y + 1][self.x].healthchange(world, add_health)
            elif self.direction == 'S':
                world.map[self.y][self.x - 1].healthchange(world, add_health)
            elif self.direction == 'W':
                world.map[self.y - 1][self.x].healthchange(world, add_health)
        if front == self.get_team():
            add_health = random.randrange(0, 4)
            if self.direction == 'N':
                world.map[self.y - 1][self.x].healthchange(world, add_health)
            elif self.direction == 'E':
                world.map[self.y][self.x + 1].healthchange(world, add_health)
            elif self.direction == 'S':
                world.map[self.y + 1][self.x].healthchange(world, add_health)
            elif self.direction == 'W':
                world.map[self.y][self.x - 1].healthchange(world, add_health)
        if left == self.get_team():
            add_health = random.randrange(0, 4)
            if self.direction == 'N':
                world.map[self.y][self.x - 1].healthchange(world, add_health)
            elif self.direction == 'E':
                world.map[self.y - 1][self.x].healthchange(world, add_health)
            elif self.direction == 'S':
                world.map[self.y][self.x + 1].healthchange(world, add_health)
            elif self.direction == 'W':
                world.map[self.y + 1][self.x].healthchange(world, add_health)

    def initialize_pattern(self, filename):
        self.movement = []
        with open(str(filename), newline='') as instr:
            instr = csv.reader(instr, delimiter='\n', quotechar='|')
            for row in instr:
                self.movement = self.movement + row
        
    def move_medic(self, world):
        x = 0
        self.pointr = self.step % self.len_instr
        if self.movement[self.pointr] != 'Right90' and self.movement[self.pointr] != 'Left90' and self.robot_infront() == 0:
            self.forward()
            x = 0
        elif self.movement[self.pointr] == 'Right90':
            self.right90()
            x = 0
        elif self.movement[self.pointr] == 'Left90':
            self.left90()
            x = 0
            
    def run_turn(self, world):
        self.heal_other()
        temp = world.map[self.y][self.x]
        world.map[self.y][self.x] = None
        self.len_instr = len(self.movement)
        x = 0
        print(self.step)
        self.move_medic(world)
        if x == 0:
            self.step += 1
        world.map[self.y][self.x] = temp
        self.robotstate = 1

def print_board(w):
    print("-" * 60)
    for y in range(19,-1,-1):
        line = ""
        for x in range(0,20):
            r = w.test_position(x, y)
            if r == None:
                line += ".... "
            else:
                if isinstance(r, AttackRobot):
                    rtype = "A"
                elif isinstance(r, MedicRobot):
                    rtype = "M"
                else:
                    rtype = "!"
                if r.get_team() == 1:
                    line += "%s%02i%s " % (rtype, r.get_health(), r.get_direction())
                else:
                    line += "%s%02i%s " % (rtype.lower(), r.get_health(), r.get_direction())
        print(line)
    print("-" * 60)

print('Robot Game')
world = World()

#add robots below

world.add_attack_robot(1, 1, 1, 'N')
world.add_attack_robot(2, 14, 10, 'S')
world.add_medic_robot(1, 1, 10, 'E', 'instr.txt')


#code to actually execute game
world.game_over()
print("Run as single-step or until completion? Step mode = 1, completion = anything else")
press = input()
if press == '1':
    cont = '0'
    while cont == '0':
        world.run_turn(world)
        print_board(world)
        print("Press 0 to continue next turn")
        cont = input()
        print(world.map)
else:
    while world.game_over() == 0:
        world.run_turn(world)
        print_board(world)
    if world.game_over() == 1:
        print("Team 1 won")
    else:
        print("Team 2 won")
    












            
            
        
