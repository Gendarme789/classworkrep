Here is the output to analyze:

Enter the numerator: abc
That wasn't a number!
I am the finally block
Enter the numerator: 100
Enter the denominator: abc
That wasn't a number!
I am the finally block
Enter the numerator: 50
Enter the denominator: 0
I am the finally block
An exception occured
integer division or modulo by zero
Press enter to exit

"Enter the numerator: abc"
This runs the try code at line 28, which calls the function "divide()" at line 1. This runs a while loop which has try, except, and finally statements. 
The try statement accepts an input numerator at lines 10 which it stores as num, and an input denominator at line 11 which it stores as denom. 
The numerator is divided by the denominator at line 15 once the inputs for both are accepted. This is stored as a result, which is then returned in line 19. 
The first input here was a string abc which cannot be divided, so an exception is raised, and the except statement at line 21 is executed.

"That wasn't a number!"
This is printed by code in line 23 due to the raised exception. 

"I am the finally block"
This is printed by the code at line 26 of the finally block from line 25.
After this, the while loop causes the try statement at line 7 to run again. 

"Enter the numerator: 100"
This stores 100 as numerator, code at line 10.

"Enter the denominator: abc"
Stores abc as denom in line 11. Causes an exception.

"That wasn't a number!"
printed by line 23 of exception statement at line 21 due to previously raised exception.

"I am the finally block"
This is printed by the code at line 26 of the finally block from line 25.
After this, the while loop causes the try statement at line 7 to run again. 

"Enter the numerator: 50"
This stores 50 as numerator, code at line 10.

"Enter the denominator: 0"
Stores 0 as denom in line 11. Result is calculated in line 15, returned in line 19.

"I am the finally block"
This is printed by the code at line 26 of the finally block from line 25.

"An exception occured"
This is printed by line 32. An exception occured in lines 29 and 30 due to the fact that denominator was 0. 
Thus the except in line 31 was triggered.

"integer division or modulo by zero"
This is line 33, which prints the actual exception e.

"Press enter to exit"
Printed by line 35. 
