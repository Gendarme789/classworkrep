def coin_count(x):
    quarters = 0
    dimes = 0
    nickels = 0
    pennies = 0
    while x > 0:
        if x >= 25:
            x = x - 25
            quarters = quarters + 1
        if 25 > x >= 10:
            x = x - 10
            dimes = dimes + 1
        if 10 > x >= 5:
            x = x - 5
            nickels = nickels + 1
        if 5 > x >= 1:
            x = x - 1
            pennies = pennies + 1
    return(quarters, dimes, nickels, pennies)

print("What is your amount of money, in dollars and cents? (for example, $3.25)")
x = float(input())
x = float(x) * 100
print("The lowest number of(quarters, dimes, nickels, pennies) you could present your cash with is:", coin_count(x))
