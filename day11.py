def letter_count(text):
    text = text.lower()
    alphabet = "abcdefghijklmnopqrstuvwxyz"

    l_count = {}

    for c in text:
        if c in alphabet:
           
            oldcount = l_count.get(c, 0)

            l_count[c] = oldcount + 1

    return l_count



def extract_key(a):
  
    return a[1]

def sort_counts(l_count):
    l_list = l_count.items()
    return sorted(l_list, key=extract_key)


l_count = letter_count("zbuycrnpuycbvzzeuyhncynypiuhujyrniztneewyiuvcnfeuyemhyunizbymiftzneycrnpupindzymruinzujyfwyzbuyvcyxnztmxneynuimxnvztpcynxjycrnpuynjatxtczinztmxyyytzcymddtptneyrimsinayxnauyhncycrnpuyzinxcrmiznztmxycwczua,yznouxydimaynyxtxuzuuxyctyzwyxtxuyrenxydmiynycwczuaymdyiuvcnfeuycrnpupindzymdyhbtpbytzyhncyzbuymxewytzuaydvxjujydmiyjukuemrauxzyyyzbuydticzymdydmviymiftzneyzuczydetsbzcymppviiujytxyxtxuzuuxyutsbzwymxuyeunjtxsyzmymruinztmxneydetsbzcyfustxxtxsytxyxtxuzuuxyutsbzwyzhmyyytzyhncyvcujymxynyzmzneymdymxuybvxjiujyzbtizwydtkuyatcctmxcydimayxtxuzuuxyutsbzwymxuyzmyzhmyzbmvcnxjyueukuxyenvxpbujydimayzbuyouxxujwycrnpuypuxzuiyytxydemitjnyyymruinztmxneyatcctmxcyenvxpbujyxvauimvcycnzueetzucytxzuirenxuzniwyrimfucynxjyzbuybvffeuycrnpuyzueucpmruypmxjvpzujycptuxpuyuyruitauxzcytxymiftzynxjyrniztptrnzujytxypmxczivpztmxynxjycuiktptxsymdyzbuytxzuixnztmxneycrnpuycznztmx")

print(sort_counts(l_count)) # The list should now be sorted

def try_decrypt(guesses, ciphertext):
    plaintext = ""
    for c in ciphertext:
    
        # This checks if c is a key within the guesses dictionary
        if c in guesses:
            plaintext += guesses[c]
        else:
            # Just copy it directly
            plaintext += c
    return plaintext

# Some test code
guesses = {"y": " ", "u": "E", "z": "T", "b": "H", "c": "S", "v": "U", "r": "P", "n": "A", "p": "C", "e": "L", "t": "I", "h": "W", "w": "Y", "i": "R", "m": "O", "x": "N", "j": "D", "f": "B", "d": "F", "a": "M", "s": "G", "o": "K", "k": "V"}
print(try_decrypt(guesses, "zbuycrnpuycbvzzeuyhncynypiuhujyrniztneewyiuvcnfeuyemhyunizbymiftzneycrnpupindzymruinzujyfwyzbuyvcyxnztmxneynuimxnvztpcynxjycrnpuynjatxtczinztmxyyytzcymddtptneyrimsinayxnauyhncycrnpuyzinxcrmiznztmxycwczua,yznouxydimaynyxtxuzuuxyctyzwyxtxuyrenxydmiynycwczuaymdyiuvcnfeuycrnpupindzymdyhbtpbytzyhncyzbuymxewytzuaydvxjujydmiyjukuemrauxzyyyzbuydticzymdydmviymiftzneyzuczydetsbzcymppviiujytxyxtxuzuuxyutsbzwymxuyeunjtxsyzmymruinztmxneydetsbzcyfustxxtxsytxyxtxuzuuxyutsbzwyzhmyyytzyhncyvcujymxynyzmzneymdymxuybvxjiujyzbtizwydtkuyatcctmxcydimayxtxuzuuxyutsbzwymxuyzmyzhmyzbmvcnxjyueukuxyenvxpbujydimayzbuyouxxujwycrnpuypuxzuiyytxydemitjnyyymruinztmxneyatcctmxcyenvxpbujyxvauimvcycnzueetzucytxzuirenxuzniwyrimfucynxjyzbuybvffeuycrnpuyzueucpmruypmxjvpzujycptuxpuyuyruitauxzcytxymiftzynxjyrniztptrnzujytxypmxczivpztmxynxjycuiktptxsymdyzbuytxzuixnztmxneycrnpuycznztmx"))
