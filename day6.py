print("Input a number")
n = input()
pi = 1
x = 0
while int(x) < int(n):
    x = int(x) + 1
    denom_1 = 2 * float(x) - 1
    denom_2 = 2 * float(x) + 1
    nom = 2 * float(x)
    next_wally = (float(nom) / float(denom_1)) * (float(nom) / float(denom_2)) 
    pi = pi * next_wally
print(float(pi * 2))
