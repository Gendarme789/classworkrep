print "Hello World"
print('print works with or without parenthesis')
print("and single or double quotes")
print("new lines can be escaped \nthis.")
print("this text will be printed"),
print("on one line because of the comma")
