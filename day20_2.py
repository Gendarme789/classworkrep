import csv
import urllib.request

url = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.csv"
webpage = urllib.request.urlopen(url)
rows = csv.reader(webpage.read().decode('utf-8').splitlines())

mag_sum = 0.0
i = 0
for row in rows:
    if i >= 1:
        mag_sum = mag_sum + float(row[4])
    else:
         pass
    i = i + 1
print("Average magnitude of Earthquakes is: ", mag_sum / i)

d = 0
n = 0
for row in rows:
    if d >= 1:
         if float(row[2]) > 130.0 or float(row[2]) < -70.0:
             n = n + 1
    else:
          pass
    d = d + 1
print(d, "earthquakes occurred in the Ring of Fire")
