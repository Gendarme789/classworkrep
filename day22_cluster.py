# A module to implement 2-dimensional k-means clustering
# See http://en.wikipedia.org/wiki/K-means_clustering

import math
import random

def dist(p1, p2):
    "Compute the distance between two points"
    return math.sqrt( (p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)

def classify(p, centroids):
    "Classify a point p by finding the closest centroid"

    # Compute the distances from p to the centroids and find the minimum
    dists = [ dist(p, c) for c in centroids ]
    m = min(dists)

    # return the index of the centroid with minimum distance
    return dists.index(m)

def classify_points(points, centroids):
    "Classifies a list of points"

    # Create a dictionary where key is 0 through k-1 and
    # the value is the list of points.
    classified_points = {}
    for c in range(len(centroids)):
        classified_points[c] = []

    # classify the points
    for p in points:
        c = classify(p, centroids)
        classified_points[c].append(p)

    return classified_points

def centroid(points):
    "Computes the centroid of a list of points"

    # The centroid is found by averaging the x and y coordinates of
    # all the points.  We use a list comprehension to extract
    # the x coodinates into a list.
    xcoords = [ p[0] for p in points]
    x = sum(xcoords) / len(points)

    # Similarly, average the y coordinates
    y = sum([ p[1] for p in points ]) / len(points)

    return (x,y)

def kmeans(k, points, iterations=20, centroids=[]):
    "Compute the k-means clustering of the points"

    if len(centroids) == 0:
        # If no centroids are given, randomly pick k of the points
        # to be the centroids.
        centroids = random.sample(points, k)

    for i in range(iterations):

        # classify the points
        classified_points = classify_points(points, centroids)

        # update the centroids
        for c in range(k):
            if len(classified_points[c]) > 0:
                centroids[c] = centroid(classified_points[c])

    return centroids, classified_points
