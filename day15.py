import tkinter
import cmath


def Mandelbrot(f, max_iter=100, cutoff=2.0, image_width=700, image_height=500, x_min=-2.0, x_max=2.0, y_min=-1.25, y_max=1.25):

    # Given an r, g, b (numbers between 0 and 255), this function creates a web label
    # color string like #2E8B57.
    def make_color(r, g, b):
        return "#%02x%02x%02x" % (r%255, g%255, b%255)

    # Use a list comprehension to create a list of colors. The result will be a
    # list of strings.  The colors I am using here is a collection of colors spanning
    # gradually from teal to aqua since that is a mix of green and blue with zero red.
    colors = [ make_color(0, i*2+40, i*2+40) for i in range(max_iter) ]

    # This function is given a complex point, and computes the color.  It does
    # this by repeatedly applying the function f (note the namespace lookups
    # needed to find f).  Once the value goes above the cutoff, the iteration
    # count is used as an index into the colors list to determine the color.
    def color_for(c):
        z = 0
        for trials in range(max_iter):
            z = z*z + c
            if abs(z) <= cutoff:
                c = z
            else:
                break
        return colors[trials]

    # Given a pixel coordinate, this function returns a complex number for the point of
    # the plane representing this pixel.
    def coord_for_pixel(pixel_x, pixel_y):
        percentX = pixel_x / float(image_width)
        percentY = pixel_y / float(image_height)
        x = x_min + percentX * (x_max - x_min)
        y = y_max - percentY * (y_max - y_min)
        return complex(x, y)

    # Use a list comprehension to create a list of (pixel_x, pixel_y, color) entries
    # The color is found by passing the return value from coord_for_pixel to color_for.
    # The list comprehension then makes px and py range through the entire grid.
    pixels = [ (px, py, color_for(coord_for_pixel(px, py))) for px in range(image_width) for py in range(image_height) ]
    return pixels


# This function uses tkinter to draw the image, you can ignore this.
def draw(pixels):
    image_width =  max([ p[0] for p in pixels])
    image_height = max([ p[1] for p in pixels])
    root = tkinter.Tk()
    im = tkinter.PhotoImage(width=image_width, height=image_height)
    for p in pixels: im.put(p[2], (p[0], p[1]))
    l = tkinter.Label(root, image=im)
    l.pack()
    root.mainloop()

# Some images

pixels = Mandelbrot(lambda x: complex(1,0.3)*cmath.sin(x), cutoff=50, x_min=-2, x_max=2)
#pixels = julia(lambda x: x * x + complex(-0.70176, -0.3842))
#pixels = julia(lambda x: x * x + complex(-0.70176, -0.3842), x_min=-0.1, x_max=0.1, y_min=-0.1, y_max=0.1)
#pixels = julia(lambda x: x * x + complex(0.285, 0.01))
#pixels = julia(lambda x: complex(1,0.4)*cmath.sin(x), cutoff=50, x_min=-2, x_max=2)


draw(pixels)
