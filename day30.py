import turtle
import math

class GeometricObject:
    def __init__(self):
        self.line_color = 'black'
    def get_color(self):
        return self.line_color
    def set_color(self, c):
        self.line_color = c

# A point is a geometric object, so we derive from it
class Point(GeometricObject):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def draw(self):
        turtle.goto(self.x, self.y)
        turtle.dot(1, self.get_color())

# A polygon is a geometric object, so we derive from it
class Polygon(GeometricObject):
    def __init__(self):
        super().__init__()
        # For a polygon, we store it as a list of points initially empty
        self.points = []

    def add_point(self, p):
        self.points.append(p)

    def draw(self):
        if len(self.points) == 0:
            return
        turtle.color(self.get_color())
        for p in self.points:
            turtle.goto(p[0], p[1])
            turtle.down()
        # Go back to the start
        turtle.goto(self.points[0][0], self.points[1][1])
        turtle.up()

# A rectangle is a polygon, so we derive from it
class Rectangle(Polygon):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.add_point( (x, y) )
        self.add_point( (x + width, y) )
        self.add_point( (x + width, y + height) )
        self.add_point( (x, y + height) )

    # The add_point method causes a "Circle-ellipse problem" and therefore violates
    # the Liskov substitution principle, since it does not make sense to add a point
    # to a rectangle.

class Square(Rectangle):
    def __init__(self, x, y, side_length):
        super().__init__(x, y, side_length, side_length)

class RightTriangle(Polygon):
    def __init__(self, left_x, left_y, base, alpha):
        super().__init__()
        self.add_point( (left_x, left_y) )
        self.add_point( (left_x + base, left_y) )
        lin_tall = base / math.cos(alpha)
        lin_tall = math.sin * lin_tall
        self.add_point( (left_x, left_y + lin_tall) )

# A canvas has many geometric objects, so we store them in a property
class Canvas:
    def __init__(self):
        self.geo = []

    def add(self, obj):
        self.geo.append(obj)

    def draw(self):
        for g in self.geo:
            g.draw()

turtle.setworldcoordinates(-5, -5, 5, 5)
turtle.up()
canvas = Canvas()

canvas.add(Rectangle(1, 2, 0.5, 0.7))
canvas.add(Point(-1, -3))

r = Rectangle(-1, 4, 2, 1)
r.set_color("green")
canvas.add(r)

sq = Square(3, 3, 1)
canvas.add(sq)

tr = RightTriangle(2, 1, 2, 1)
canvas.add(tr)

canvas.draw()
input("Press enter")
