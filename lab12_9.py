def median(L):
    L.sort()
    if len(L) % 2 == 1: # odd length
        return L[int(len(L)/2)]
    else:
        a = L[int(len(L)/2 - 1)]
        b = L[int(len(L)/2)]
        return (b+a)/2.0

L = [0, -1, 1, 2, -5, 6, -10]

print(median(L))
