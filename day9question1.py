import math
import turtle
import tkinter

print("Let's draw a rectangle! first, we need to know where it starts. What is its x coordinate?")
x = input()
x = float(x)
print("Alright, what is its y coordinate?")
y = input()
y = float(y)
print("How tall do you want the rectangle to be?")
height = input()
height = float(height)
print("How wide do you want the rectangle to be?")
width = input()
width = float(width)

nh = float(y) + float(height)
nw = float(x) + float(width)

def draw_rectangle(x, y):
    turtle.up()
    turtle.goto(x, y)
    turtle.down()
    turtle.begin_fill()
    turtle.color( (1.0,0.0,0.0), (1.0, 0.0, 0.0))
    turtle.goto(x, nh)
    turtle.goto(nw, nh)
    turtle.goto(nw, y)
    turtle.goto(x, y)

draw_rectangle(x, y)

