import turtle
from day27_world import World

turtle.setworldcoordinates(0,0,25,25)

w = World(xsize=25, ysize=25, numfish=10, numbears=10)
w.draw()

while True:
    turns = int(input("Number of turns to advance (0 to exit): "))
    if turns == 0:
        break
    for i in range(turns):
        w.live_a_little()
    w.draw()
