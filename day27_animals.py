import random
import turtle

class Animal:
    "A base class representing either a fish or a bear"

    def __init__(self, shapename, x, y, world):
        "Takes as input a picture and the current x and y coordinates"

        self.world = world
        self.shapename = shapename
        self.turtle = None
        self.x = x
        self.y = y
        self.breed_tick = 0

    def draw(self):
        "Draws the animal"
        if self.turtle == None:
            self.turtle = turtle.Turtle()
            self.turtle.up()
            self.turtle.shape(self.shapename)
            self.turtle.hideturtle()
        self.turtle.goto(self.x, self.y)
        self.turtle.showturtle()

    def kill(self):
        "Kills the animal by removing the turtle from the screen"
        if self.turtle != None:
            self.turtle.hideturtle()

    def try_to_move(self):
        "Try and move to an adjacent location as long as it isn't empty"

        # the list of possible offsets from the current position
        offsets = [(-1,1),  (0,1),  (1,1),
                   (-1,0),          (1, 0),
                   (-1,-1), (0,-1), (1,-1)]

        # pick a random offset
        randoffset = random.choice(offsets)
        newx = self.x + randoffset[0]
        newy = self.y + randoffset[1]

        # move if the new location is empty
        if self.world.is_empty(newx, newy):
            self.world.move(self, newx, newy)
            self.x = newx
            self.y = newy

    def try_to_breed(self, AnimalType):
        "Try to breed by adding a new animal to a random location" 

        # pick a random offset
        offsets = [(-1,1),  (0,1),  (1,1),
                   (-1,0),          (1, 0),
                   (-1,-1), (0,-1), (1,-1)]
        randoffset = random.choice(offsets)
        testx = self.x + randoffset[0]
        testy = self.y + randoffset[1]

        # create a new animal there if the location is empty
        if self.world.is_empty(testx, testy):
            newanimal = AnimalType(testx, testy, self.world)
            self.world.add_animal(newanimal)
            self.breed_tick = 0

    def live_a_little(self):
        print("Error: live_a_little must be implemented in a derived class")

class Fish(Animal):

    def __init__(self, x, y, world):
        Animal.__init__(self, "square", x, y, world)

    def live_a_little(self):
        "Make the fish live for a little bit of time"

        # Count up the number of adjacent fish by looping through
        # all offsets from the current position.
        offsets = [(-1,1),  (0,1),  (1,1),
                   (-1,0),          (1, 0),
                   (-1,-1), (0,-1), (1,-1)]
        adjfish = 0
        for offset in offsets:
            testx = self.x + offset[0]
            testy = self.y + offset[1]
            if self.world.has_fish(testx, testy):
                adjfish += 1

        # If there are two adjacent fish, this fish dies.
        if adjfish >= 2:
            self.world.kill_animal(self.x, self.y)
            return

        # If the fish is still alive, increment its breed tick
        self.breed_tick += 1
        # If it has been 10 turns since the fish bred, we try and breed
        if self.breed_tick >= 10:
            self.try_to_breed(Fish)

        self.try_to_move()

class Bear(Animal):

    def __init__(self, x, y, world):
        Animal.__init__(self, "triangle", x, y, world)
        self.starve_tick = 0
        self.starve_lim = random.randrange(16, 25)

    def live_a_little(self):
        "Make the bear live for a little bit of time"

        # Try and breed
        self.breed_tick += 1
        if self.breed_tick >= 21:
            self.try_to_breed(Bear)

        self.try_to_eat()

        # If the bear has not eaten in some turns, kill it.
        if self.starve_tick >= self.starve_lim:
            self.world.kill_animal(self.x, self.y)
            return

        self.try_to_move()

    def try_to_eat(self):

        # compute the list of adjacent prey by looping through all offsets
        offsets = [(-1,1),  (0,1),  (1,1),
                   (-1,0),          (1, 0),
                   (-1,-1), (0,-1), (1,-1)]

        adjfish = []
        for offset in offsets:
            testx = self.x + offset[0]
            testy = self.y + offset[1]
            if self.world.has_fish(testx, testy):
                adjfish.append( (testx, testy))

        # If there are no fish, increment the starve tick
        if len(adjfish) == 0:
            self.starve_tick += 1
            return

        # Pick a random fish to eat
        randfish = random.choice(adjfish)
        self.starve_tick = 0
        self.world.kill_animal(randfish[0], randfish[1])
