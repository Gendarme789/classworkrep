def mean(L):
    total = 0.0
    for x in L:
        total = total + x
    return total/len(L)

L = [0, -1, 1, 3, -5, 6, -10]

print(mean(L))
