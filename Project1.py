def make_key(x):
    L = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    x = str.upper(x)
    x = x.replace('J', 'I')
    n = 0
    for i in x:
        if i in L:
            pass
        elif i == ' ':
            pass
        else:
            L[n] = i
            n = n + 1
    y = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for i in y:
        if i in L:
            pass
        else:
            L[n] = i
            n = n + 1
    A = L[0:5]
    B = L[5:10]
    C = L[10:15]
    D = L[15:20]
    E = L[20:25]
    table = [A,
             B,
             C,
             D,
             E]
    return(table)

def make_strng(y):
    D = {}
    y = str.upper(y)
    y = y.replace('J', 'I')
    y = y.replace(' ', '')
    Double = ['AA', 'BB', 'CC', 'DD', 'EE', 'FF', 'GG', 'HH', 'II', 'KK', 'LL', 'MM', 'NN', 'OO', 'PP', 'QQ', 'RR', 'SS', 'TT', 'UU', 'VV', 'WW', 'XX', 'YY', 'ZZ']
    Replace = ['AXA', 'BXB', 'CXC', 'DXD', 'EXE', 'FXF', 'GXG', 'HXH', 'IXI', 'KXK', 'LXL', 'MXM', 'NXN', 'OXO', 'PXP', 'QXQ', 'RXR', 'SXS', 'TXT', 'UXU', 'VXV', 'WXW', 'XQX', 'YXY', 'ZXZ']
    for i in Double:
        if i in y:
            y = y.replace(i, Replace[Double.index(i)])
        else:
            pass
    for i in Double:
        if i in y:
            y = y.replace(i, Replace[Double.index(i)])
        else:
            pass
    if len(y) % 2 == 0:
        pass
    elif len(y) % 2 == 1 and y[len(y) - 1] != 'Z':
        y = y + 'Z'
    elif len(y) % 2 == 1 and y[len(y) - 1] == 'Z':
        y = y + 'Q'
    return y

def find_letter(letter):
    Row = 0
    Col = 0
    for row in range(5):
        for col in table[row]:
            if col == letter:
                Col = int(table[row].index(str(letter)))
                Row = int(row)
    return Row, Col

def encode_pair(a, b):
    c = find_letter(a)
    d = find_letter(b)
    if a == b:
        return("ERROR: letters to encode_pair must be distinct")
    elif c[0] == d[0]:
        if c[1] == 4:
            return table[c[0]][0] + table[d[0]][int(d[1]+1)]
        elif d[1] == 4:
            return table[c[0]][int(c[1]+1)] + table[d[0]][0]
        else:
            return table[c[0]][int(c[1]+1)] + table[d[0]][int(d[1]+1)]
    elif c[1] == d[1]:
        if c[0] == 4:
            return table[0][c[1]] + table[int(d[0]+1)][d[1]]
        elif d[0] == 4:
            return table[int(c[0]+1)][c[1]] + table[0][d[1]]
        else:
            return table[int(c[0]+1)][c[1]] + table[int(d[0]+1)][d[1]]
    else:
        if c[0] < d[0]:
            return table[c[0]][d[1]] + table[d[0]][c[1]]
        else:
            return table[c[0]][d[1]] + table[d[0]][c[1]]

def decode_pair(a, b):
    c = find_letter(a)
    d = find_letter(b)
    if a == b:
        return("ERROR: letters to encode_pair must be distinct")
    elif c[0] == d[0]:
        if c[1] == 4:
            return table[c[0]][c[0]-2] + table[d[0]][int(d[1]-1)]
        elif d[1] == 4:
            return table[c[0]][int(c[1]-1)] + table[d[0]][d[0]-2]
        else:
            return table[c[0]][int(c[1]-1)] + table[d[0]][int(d[1]-1)]
    elif c[1] == d[1]:
        if c[0] == 4:
            return table[0][c[1]] + table[int(d[0]-1)][d[1]]
        elif d[0] == 4:
            return table[int(c[0]-1)][c[1]] + table[0][d[1]]
        else:
            return table[int(c[0]-1)][c[1]] + table[int(d[0]-1)][d[1]]
    else:
        if c[0] < d[0]:
            return table[c[0]][d[1]] + table[d[0]][c[1]]
        else:
            return table[c[0]][d[1]] + table[d[0]][c[1]]

def exe_encode(y):
    i = 0
    d = 0
    enc = str()
    while i < len(y):
        d = encode_pair(y[i], y[i + 1])
        enc = enc + str(d) + ' '
        i = i + 2
    return(enc)

def exe_decode(y):
    i = 0
    d = 0
    enc = str()
    y = y.replace(' ', '')
    while i < len(y):
        d = decode_pair(y[i], y[i + 1])
        enc = enc + str(d) + ' '
        i = i + 2
    enc = enc.replace(' ', '')
    Double = ['AA', 'BB', 'CC', 'DD', 'EE', 'FF', 'GG', 'HH', 'II', 'KK', 'LL', 'MM', 'NN', 'OO', 'PP', 'QQ', 'RR', 'SS', 'TT', 'UU', 'VV', 'WW', 'XX', 'YY', 'ZZ']
    Replace = ['AXA', 'BXB', 'CXC', 'DXD', 'EXE', 'FXF', 'GXG', 'HXH', 'IXI', 'KXK', 'LXL', 'MXM', 'NXN', 'OXO', 'PXP', 'QXQ', 'RXR', 'SXS', 'TXT', 'UXU', 'VXV', 'WXW', 'XQX', 'YXY', 'ZXZ']
    for i in Replace:
        if i in enc:
            enc = enc.replace(i, Double[Replace.index(i)])
        else:
            pass
    for i in Replace:
        if i in enc:
            enc = enc.replace(i, Double[Replace.index(i)])
        else:
            pass
    return(enc)

print("What is your string for a key?")
x = input(str())
print("What is the string you want to encode?")
y = input(str())
print("Encoding...")
table = make_key(x)
y = make_strng(y)
enc = exe_encode(y)
print(enc)
enc = exe_decode(enc)
print(enc)


    
