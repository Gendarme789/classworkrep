def leibnitz(n):
    pi = 0.0
    n = 1000
    for i in range(n):
        denom = i * 2 + 1
        pi = pi + 4.0/denom * (-1)**i
    return(pi)

def wallis(n):
    pi = 1.0
    x = 0
    n = 1000
    while int(x) < int(n):
        x = int(x) + 1
        denom_1 = 2 * float(x) - 1
        denom_2 = 2 * float(x) + 1
        nom = 2 * float(x)
        next_wally = (float(nom) / float(denom_1)) * (float(nom) / float(denom_2)) 
        pi = pi * next_wally
    pi = pi * 2
    return(pi)

print("This program finds pi using the Wallis and Leibnitz methods. What is the number of iterations you want to use?")
n = input()
print(leibnitz(n))
print(wallis(n))
