import day22_cluster
import turtle
import csv
import urllib.request

# Some sample data
url = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.csv"
webpage = urllib.request.urlopen(url)
rows = csv.reader(webpage.read().decode('utf-8').splitlines())
points = []
i = 0
for row in rows:
    if i > 0:
        l = [0, 0]
        first = row[2]
        first = float(first)
        second = row[3]
        second = float(second)
        l[0] = first
        l[1] = second
        l = tuple(l)
        points.append(l)
    else:
        pass
    i = i + 1

centroids = [(0, 130), (0, 30), (0, -70)]
# 3 groups seem to work best: earthquakes greater than 130, lesser than -70 are in ring of fire.
# third group represents those outside of ring of fire. 

colors = ["red", "green", "blue"]

turtle.setworldcoordinates(20, 0, 60, 40)
turtle.hideturtle()
turtle.up()

# Draw the points and the initial centroids
for p in points:
    turtle.goto(p[0], p[1])
    turtle.dot()
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(10)

input("Press enter to continue")

# Classify the points
classified_points = day22_cluster.classify_points(points, centroids)

# draw the classification
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

# Run a single iteration
centroids, classified_points = day22_cluster.kmeans(3, points, 2, centroids)

# draw the new centroids
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(14)

input("Press enter to continue")

# Draw the new classification
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

input("Press enter to continue")

# clear the screen
turtle.clear()

# Run 20 iterations starting from random points
centroids, classified_points = day22_cluster.kmeans(3, points)

# draw the classification
for i in range(3):
    turtle.color(colors[i])
    turtle.goto(centroids[i])
    turtle.dot(14)
for c in classified_points.keys():
    turtle.color(colors[c])
    for p in classified_points[c]:
        turtle.goto(p[0], p[1])
        turtle.dot()

input("Press enter to exit")
