names = ['Joe', 'Tom', 'Bob', 'Emily', 'Sue']
scores = [10, 23, 13, 18, 12]
scores_dict = {}
print(len(scores))

def make_scores_dict(x, y):
    z = 0
    n = -1
    while z < len(scores):
        z = z + 1
        n = n + 1
        scores_dict[names[n]] = scores[n]
    return scores_dict

make_scores_dict(names, scores)
print(scores_dict)

print(scores_dict['Emily'])

scores_dict['Diane'] = 19
print(scores_dict)

scores_dict['Sue'] = 14
print(scores_dict)

del scores_dict['Tom']
print(scores_dict)

scores_total = scores_dict['Emily'] + scores_dict['Sue'] + scores_dict['Bob'] + scores_dict['Joe']
print(scores_total / 4)






