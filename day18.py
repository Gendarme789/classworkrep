import csv

with open('2.5_week.csv', newline='') as f:
    rows = csv.reader(f)
    mag_sum = 0.0
    i = 0
    for row in rows:
        if i >= 1:
            mag_sum = mag_sum + float(row[4])
        else:
            pass
        i = i + 1
    print("Average magnitude of Earthquakes is: ", mag_sum / i)

with open('2.5_week.csv', newline='') as y:
    rowsb = csv.reader(y)
    d = 0
    n = 0
    for rowb in rowsb:
        if d >= 1:
            if float(rowb[2]) > 130.0 or float(rowb[2]) < -70.0:
                n = n + 1
        else:
            pass
        d = d + 1
    print(d, "earthquakes occurred in the Ring of Fire")
