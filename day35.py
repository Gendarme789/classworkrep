def selection_sort(L):
    for i in range(0, len(L)):

        # Find the smallest among i...len(L)
        min_idx = i
        for j in range(i+1,len(L)):
            if L[j] < L[min_idx]:
                min_idx = j

        # Move smallest into position i
        temp = L[min_idx]
        L[min_idx] = L[i]
        L[i] = temp

import timeit
import random
L = [random.randint(0,10000) for i in range(3400000)]

timeit.timeit('[random.randint(0,10000) for i in range(3400000)].sort()', number=10000)




