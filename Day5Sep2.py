term = int(input("Compute which Lucas number? "))
if term == 0:
    print(2)

elif term == 1:
    print(1)

elif term < 0:
    print("Number cannot be negative")

else:
    f_prev = 1
    f_current = 3
    i = 2
    while i < term:
        temp = f_current + f_prev
        f_prev = f_current
        f_current = temp
        i = i + 1
    print(f_current)