def count_nonneg(L):
    c = 0
    for x in L:
        if x >= 0:
            c = c + 1
    return c

L = [0, -1, 1, 3, -5, 6, -10]

print(count_nonneg(L))
